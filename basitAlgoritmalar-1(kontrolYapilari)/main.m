//
//  main.m
//  basitAlgoritmalar-1(kontrolYapilari)
//
//  Created by Yakup on 13.07.2017.
//  Copyright © 2017 Yakup. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <math.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
       //Negatif sayı tespiti, negatif değilse sayının karakökünü alan uygulama
        
        double sayi;
        NSLog(@"\nBir Sayi Giriniz: ");
        scanf("%lf",&sayi);
        
        if(sayi<0)
            NSLog(@"Sayi 0 küçüktür.");
        else
        {
          NSLog(@"Sayinin Kara kökü = %f",sqrt(sayi));
        }
        
    }
    return 0;
}
